<?php


/**
 * Set Version Compatibility
 */
if ( version_compare( $GLOBALS['wp_version'], '3.6', '<' ) ) {
  require get_template_directory() . '/inc/back-compat.php';
}

/**
 * Custom Login Page Logo
 */
function levlabs_login_logo() { ?>

  <style type="text/css">
    body.login #login h1 a {
      background-image: url( <?php echo get_stylesheet_directory_uri() . 'images/image.png'; ?> );
    }
  </style>

<?php }

add_action( 'login_enqueue_scripts', 'wpptl_login_logo' );

?>
